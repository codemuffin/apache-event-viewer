## Debug Apache Events

If you're running Apache as a Windows service, you can debug it with **Window's Event Viewer**.

To see the log:

- Open the Run dialog and enter `eventvwr.msc` (you can also search for "Event Viewer" from the start menu)
- Expand Windows Logs and click Application
- Check the Source column in the log. Look for Apache Service
- Click on any errors you see
- Look at the panel at the bottom. Errors are shown here

In the screenshot below, the issue was caused my vhosts config pointing to a directory that doesn't exist.

You can also create a custom view to keep all your Apache event logs in one place:

- Download the Apache Events config
- Import it by right-clicking Event Viewer (Local) > Import Custom View > Choose the downloaded XML